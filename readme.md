Hello Sprout Social Team,

I consider this the first iteration off of the wireframe for the Designer Developer Candidate Challenge Asignment. Give the time constraints and working with my existing capabilities, this repo represents a fair assessment of what I can do with given requirements and base. I am awaiting feedback on how this could be further improved upon and consider this to be a next step prototype and not product ready code.

The tooling I chose to use is simply `npm` and `node-sass` to compile the styles. The output CSS has already been checked into the repo but if you wish to compile, it can be done so with these steps.

`npm install`

then

`npm run dev`

to build the `.scss` into `css`

A simple node/python/ruby/etc server of the project directory can serve the `index.html` or simply it can be directly opened.

To be honest, I had difficulty with the JavaScript provided as I included `underscore` but couldn't figure out, for a bit, why the methods were not producing the results I wanted. I then realized that `lodash` was the library providing the utility methods to compare the the form data to the static plans data object.

The visual design I went with is what I believe matches Sprout Social's existing branding, while including a little bit of my own flair.

Some technical caveats, I considered but were not implemented, are the input number validation and a "back button" or redo feature to input different numbers for a different result.